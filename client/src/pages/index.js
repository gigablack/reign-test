import React from "react"
import '../globalStyle/style.scss'
import Header from '../Components/Header/Header.jsx'
import List from '../Components/List/List.jsx'
import { graphql } from 'gatsby'

export default function Home({ data }) {
  return (
    <main>
      <title>HN Feed</title>
      <Header />
      <List apiUrl={data.site.siteMetadata.api_url} />
    </main>
  )
}

export const query = graphql`
query Api {
  site {
    siteMetadata {
      api_url
    }
  }
}

`
