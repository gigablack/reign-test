import React, { useState, useEffect } from 'react'
import StoryItem from '../StoryItem/StoryItem.jsx'

const List = ({ apiUrl }) => {
  const [stories,setStories] = useState([])
  const makeRequest = async () => {
    try {
      let response = await fetch(`${apiUrl}/api`)
      let res = await response.json()
      let parsedRes = res.map(story => {
        return {
          ...story,
          date: new Date(story.date).getTime()
        }
      })
      setStories(parsedRes.sort((a,b) => b.date - a.date ))
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    if(stories.length === 0){
      makeRequest()
    }
  },[])
    return (
        <section>
            {stories.map(story => <StoryItem key={story.url} story={story} apiUrl={apiUrl} stories={stories} setStories={setStories} />)}
        </section>
    )
}

export default List