import React,{ useState } from 'react'
import DeleteButton from '../DeleteButton/DeleteButton.jsx'
import StoryAuthor from '../StoryAuthor/StoryAuthor.jsx'
import StoryDate from '../StoryDate/StoryDate.jsx'
import StoryTitle from '../StoryTitle/StoryTitle.jsx'
import { container, titleAndAuthor, dateAndButton } from './StoryItem.module.scss'

const StoryItem = ({ story, apiUrl, stories, setStories }) => {
    const [isVisible,setIsVisible] = useState(false)
    return (
        <li className={container} onMouseOver={() => setIsVisible(true)} onMouseOut={() => setIsVisible(false)}>
            <article>
                <div className={titleAndAuthor}>
                    <StoryTitle storyTitle={story.title} storyUrl={story.url} />
                    <StoryAuthor storyAuthor={story.author} />
                </div>
                <div className={dateAndButton}>
                    <StoryDate storyDate={story.date} />
                    <DeleteButton isVisible={isVisible} storyObjId={story._id} apiUrl={apiUrl} stories={stories} setStories={setStories} />
                </div>
            </article>
        </li>
    )
}

export default StoryItem