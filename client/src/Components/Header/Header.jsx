import React from 'react'
import { container } from './Header.module.scss'

const Header = () => {
    return (
        <header className={container}>
            <h1>HN Feed</h1>
            <h2>We { '<3' } hacker news!</h2>
        </header>
    )
}

export default Header