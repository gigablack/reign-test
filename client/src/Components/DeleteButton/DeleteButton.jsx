import React from 'react'
import { FaTrash } from '@react-icons/all-files/fa/FaTrash'
import { buttonClass } from './DeleteButton.module.scss'
import Swal from 'sweetalert2'

const DeleteButton = ({ isVisible, storyObjId, apiUrl, stories, setStories }) => {

  const updateStoriesList = (id) => {
    setStories(stories.filter(story => story._id !== id))
  }

  const deleteStory = async (storyObjId) => {
    try {
      let response = await fetch(`${apiUrl}/api/${storyObjId}`,{
        method: 'PUT'
      })
      let res = await response.json()
      if(res){
        Swal.fire(
          'Deleted!',
          'The story has been deleted.',
          'success'
        )
        updateStoriesList(storyObjId)
      }
    } catch (error) {
      console.log(error)
      Swal.fire({
        icon: 'error',
        title: 'Oops!',
        text: 'Server Error'
      })
    }
  }
    const handleClick = () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
              deleteStory(storyObjId)        
            }
          })
    }
    return (
        <button disabled={!isVisible} className={buttonClass} onClick={handleClick} style={ !isVisible ? { color: 'transparent' } : null } ><FaTrash size={20} /></button>
    )
}

export default DeleteButton