import React from 'react'
import { title } from './StoryTitle.module.scss'

const StoryTitle = ({ storyTitle, storyUrl }) => {
    return (
        <p className={title}><a href={storyUrl} target="_blank">{storyTitle}</a></p>
    )
}

export default StoryTitle