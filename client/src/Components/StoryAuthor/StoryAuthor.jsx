import React from 'react'
import { authorClass } from './StoryAuthor.module.scss'

const StoryAuthor = ({ storyAuthor }) => {
    return (
        <p className={authorClass}>- {storyAuthor} -</p>
    )
}

export default StoryAuthor