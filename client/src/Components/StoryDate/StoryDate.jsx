import React from 'react'
import { dateClass } from './StoryDate.module.scss'
import Moment from 'react-moment'

const StoryDate = ({ storyDate }) => {
    const calendarStrings = {
        lastDay : '[Yesterday]',
        sameDay : 'h:mm a',
        lastWeek: 'MMM D',
        sameElse : 'MM/DD/YY'
    };
    return (
        <p className={dateClass}><Moment calendar={calendarStrings} date={storyDate} /></p>
    )
}

export default StoryDate