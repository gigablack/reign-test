/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

 const api_url = process.env.GATSBY_API_HOST || 'http://localhost'

module.exports = {
  siteMetadata: {
    api_url: api_url
  },
  /* Your site config here */
  plugins: [`gatsby-plugin-sass`],
}
