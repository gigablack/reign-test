import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Story, StorySchema } from './schemas/story.schema'
import { ScheduleModule } from '@nestjs/schedule'

const {
  DB_USER,
  DB_PASS,
  DB_HOST,
  DB_PORT
} = process.env

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}`),
    MongooseModule.forFeature([{ name: Story.name, schema: StorySchema }]),
    ScheduleModule.forRoot()
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
