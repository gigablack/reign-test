import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose'
import { Story, StoryDocument, StorySchema } from './schemas/story.schema'
import { SchemaType, Types } from 'mongoose';
import { types } from 'util';

const {
  DB_USER,
  DB_PASS,
  DB_HOST,
  DB_PORT
} = process.env

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(`mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}`),
        MongooseModule.forFeature([{ name: Story.name, schema: StorySchema }])
      ],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  /* describe('root', () => {
    it('should return a StoryDocument Array', async () => {
      expect(await appController.updateStory()).toContain(Types.DocumentArray);
    });
  }); */
});
