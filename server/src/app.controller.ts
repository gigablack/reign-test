import { Controller, Get, Param, Put } from '@nestjs/common';
import { AppService } from './app.service';
import { StoryDocument } from './schemas/story.schema';

@Controller('/api')
export class AppController {
  constructor(private readonly appService: AppService) {
    this.appService.makeRequest()
  }

  @Get()
  async getAll(): Promise<StoryDocument[]> {
    return this.appService.findAll()
  }

  @Put(':id')
  async updateStory(@Param() params): Promise<StoryDocument> {
    return this.appService.updateStory(params.id)
  }
}
