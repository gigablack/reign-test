import { Injectable } from '@nestjs/common';
import { Model, Schema } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Story, StoryDocument } from './schemas/story.schema';
import { Cron, CronExpression } from '@nestjs/schedule';
import { CreateStoryDto } from './schemas/dto/create-story.dto';
import fetch from 'node-fetch';

@Injectable()
export class AppService {

  constructor(@InjectModel(Story.name) private storyModel: Model<StoryDocument> ) {}

  @Cron(CronExpression.EVERY_HOUR)
  async makeRequest(): Promise<void> {
    try {
      let response = await fetch('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      let res = await response.json()
      let stories = res.hits.filter(hit => (hit.title || hit.story_title) && (hit.story_url || hit.url) && hit.story_id)
      for (let story of stories){
        let docs = await this.storyModel.aggregate().match({ story_id: { $eq: story.story_id } })
        if(docs.length === 0){
          const { author, title, story_title, story_url, url, story_id, created_at  } = story
          let newStory: CreateStoryDto = {
            author,
            date: created_at,
            title: story_title || title,
            story_id,
            url: story_url || url
          }
          let newCreatedStory = new this.storyModel(newStory)
          await newCreatedStory.save()
        }
      }
    } catch (error) {
      console.log(error)
    } 
  }

  async findAll(): Promise<StoryDocument[]> {
    let docs = await this.storyModel.find({ is_deleted: false }).exec()
    return docs
  }

  async updateStory(id: Schema.Types.ObjectId): Promise<StoryDocument> {
    try {
      let storyUpdated = await this.storyModel.updateOne({ _id: id },{ is_deleted: true })
      return storyUpdated
    } catch (error) {
      console.log(error)
    }
  }

  getHello(): string {
    return 'Hello World!';
  }
}
