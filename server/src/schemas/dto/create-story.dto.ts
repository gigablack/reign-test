export class CreateStoryDto {
    author: string;
    date: string;
    story_id: number;
    title: string;
    url: string;
}