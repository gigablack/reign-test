import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type StoryDocument = Story & mongoose.Document;

@Schema()
export class Story {
    @Prop()
    author: string;

    @Prop({ type: mongoose.Schema.Types.Date })
    date: string;

    @Prop()
    story_id: number;

    @Prop()
    title: string;

    @Prop()
    url: string;

    @Prop({ default: false })
    is_deleted: boolean;
}

export const StorySchema = SchemaFactory.createForClass(Story);