# Full Stack Developer Test

## Requirements
- Docker
- Docker-Compose

## Quick Start
1. ```git clone https://gitlab.com/gigablack/reign-test.git```
2. ```cd reign-test```
3. ```docker-compose up```
3. go to http://localhost:80
    - The entire app works under a nginx reverse proxy to handle all the requests to the client and the server.

## Start in Development Mode
```docker-compose -f docker-compose.dev.yml up```

## Eviroments Variables
1. DATABASE:
    - MONGO_INITDB_ROOT_USERNAME
    - MONGO_INITDB_ROOT_PASSWORD
2. SERVER:
    - DB_HOST
    - DB_PORT
    - DB_USER
    - DB_PASS
3. CLIENT:
    - API_HOST